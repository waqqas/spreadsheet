import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class SpreadSheet {
	private int rows;
	private int cols;
	private HashMap<String, Double> cellValues = new HashMap<>();
	private String[] sheetNames;
	private double[][] data;

	public SpreadSheet(int rows, int cols) {
		if(rows < 1 || cols < 1) {
			return;
		}
		this.rows = rows;
		this.cols = cols;
		this.data = new double[rows][cols];
		this.sheetNames = new String[cols];

		char[] alphabets = "abcdefghijklmnopqrstuvwxyz".toCharArray();

		for (int i = 0; i < cols; i++) {
			char c = alphabets[i];
			this.sheetNames[i] = c + "";
		}
		
		//initialize all cell values with 0.0
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				this.data[i][j] = 0.0;
				String cellName = sheetNames[j] + (i + 1);
				getCellValues().put(cellName, 0.0);
			}
		}
	}
	
	int getCols() {
		return cols;
	}

	void setCols(int cols) {
		this.cols = cols;
	}

	int getRows() {
		return rows;
	}

	void setRows(int rows) {
		this.rows = rows;
	}

	void initializeData() {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				String cellName = sheetNames[j] + (i + 1);
				System.out.print(cellName + " = ");

				// take input for a particular cell
				String value = scanner.next();
				value = value.trim();
				value = value.replace(" ", "");

				// if cell value is numeric
				if (isNumeric(value)) {
					data[i][j] = new Double(value);
					getCellValues().put(cellName, data[i][j]);
				} else {
					// if cell value is a expression
					String[] dataTokens = value.split("[-+*/]");
					for (String token : dataTokens) {
						// replace cells by their values
						if (getCellValues().containsKey(token)) {
							value = value.replace(token, getCellValues().get(token).toString());
						}
					}
					ScriptEngineManager mgr = new ScriptEngineManager();
					ScriptEngine engine = mgr.getEngineByName("JavaScript");
					try {
						// evaluate the expression in one go
						double result;
						if (value.contains(".")) {
							result = (double) engine.eval(value);
						} else {
							result = (int) engine.eval(value);
						}
						data[i][j] = result;
						getCellValues().put(cellName, result);
					} catch (ScriptException e) {
						e.printStackTrace();
					}
				}
			}
		}
		scanner.close();
		displayData();
	}
	
	void initializeDataFromString(ArrayList<String> input) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				String cellName = sheetNames[j] + (i + 1);
				
				// take input for a particular cell
				String value = input.remove(0);
				value = value.trim();
				value = value.replace(" ", "");

				// if cell value is numeric
				if (isNumeric(value)) {
					data[i][j] = new Double(value);
					getCellValues().put(cellName, data[i][j]);
				} else {
					// if cell value is a expression
					String[] dataTokens = value.split("[-+*/]");
					for (String token : dataTokens) {
						// replace cells by their values
						if (getCellValues().containsKey(token)) {
							value = value.replace(token, getCellValues().get(token).toString());
						}
					}
					ScriptEngineManager mgr = new ScriptEngineManager();
					ScriptEngine engine = mgr.getEngineByName("JavaScript");
					try {
						// evaluate the expression in one go
						double result;
						if (value.contains(".")) {
							result = (double) engine.eval(value);
						} else {
							result = (int) engine.eval(value);
						}
						data[i][j] = result;
						getCellValues().put(cellName, result);
					} catch (ScriptException e) {
						e.printStackTrace();
					}
				}
			}
		}
		scanner.close();
		displayData();
	}

	private void displayData() {
		// print the col names
		for (int i = 0; i < cols; i++) {
			System.out.print("\t" + sheetNames[i].toUpperCase() + " ");
		}
		System.out.println();

		for (int i = 0; i < rows; i++) {
			// print the row number
			System.out.print(i + 1 + "\t");
			for (int j = 0; j < cols; j++) {
				// print the cell value
				System.out.print(data[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public HashMap<String, Double> getCellValues() {
		return cellValues;
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String input = scanner.next();

		if (input.length() < 2 || !input.contains("*")) {
			System.out.println("Invalid input!. Please try again (row*col):");
			return;
		}

		String[] inputTokens = input.split("\\*");

		int rows = 1, cols = 1;
		try {
			rows = new Integer(inputTokens[0]);
		}
		catch(Exception e) {
			System.out.println("Invalid input!. Please try again (row*col):");
			return;
		}
		try {
			cols = new Integer(inputTokens[1]);
		}
		catch(Exception e) {
			System.out.println("Invalid input!. Please try again (row*col):");
			return;
		}
		
		if (rows < 1 || cols < 1) {
			System.out.println("Invalid input!. Please try again (row*col):");
			return;
		}

		SpreadSheet ss = new SpreadSheet(rows, cols);
		ss.initializeData();
	}

	
}
