import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpreadSheetTest {
	@Test
	public void numberOfColsAreGreaterThanNumberofRows() {
		ArrayList<String> input = new ArrayList<String>();
		input.add("1");
		input.add("2");
		input.add("3");
		input.add("4");
		input.add("5");
		input.add("6");
		input.add("7");
		input.add("8");
		SpreadSheet ss = new SpreadSheet(2, 4);
		ss.initializeDataFromString(input);
		assertTrue(ss.getCellValues().get("a1") == 1);
		assertTrue(ss.getCellValues().get("b1") == 2);
		assertTrue(ss.getCellValues().get("c1") == 3);
		assertTrue(ss.getCellValues().get("d1") == 4);
		assertTrue(ss.getCellValues().get("a2") == 5);
		assertTrue(ss.getCellValues().get("b2") == 6);
		assertTrue(ss.getCellValues().get("c2") == 7);
		assertTrue(ss.getCellValues().get("d2") == 8);
	}
	
	@Test
	public void initializeCellWithConstants() {
		ArrayList<String> input = new ArrayList<String>();
		input.add("1");
		input.add("2");
		input.add("3");
		input.add("4");
		SpreadSheet ss = new SpreadSheet(2, 2);
		ss.initializeDataFromString(input);
		assertTrue(ss.getCellValues().get("a1") == 1);
		assertTrue(ss.getCellValues().get("b1") == 2);
		assertTrue(ss.getCellValues().get("a2") == 3);
		assertTrue(ss.getCellValues().get("b2") == 4);
	}
	
	@Test
	public void initializeCellWithCellValue() {
		ArrayList<String> input = new ArrayList<String>();
		input.add("2");
		input.add("3");
		input.add("a1 + b1");
		input.add("a2 * 2");
		SpreadSheet ss = new SpreadSheet(2, 2);
		ss.initializeDataFromString(input);
		assertTrue(ss.getCellValues().get("a1") == 2);
		assertTrue(ss.getCellValues().get("b1") == 3);
		assertTrue(ss.getCellValues().get("a2") == 5);
		assertTrue(ss.getCellValues().get("b2") == 10);
	}
	
	@Test
	public void initializeCellWithCellValueAndConstant() {
		ArrayList<String> input = new ArrayList<String>();
		input.add("2");
		input.add("3");
		input.add("a1 + b1 + 2");
		input.add("a2 * 2 + 2");
		SpreadSheet ss = new SpreadSheet(2, 2);
		ss.initializeDataFromString(input);
		assertTrue(ss.getCellValues().get("a1") == 2);
		assertTrue(ss.getCellValues().get("b1") == 3);
		assertTrue(ss.getCellValues().get("a2") == 7);
		assertTrue(ss.getCellValues().get("b2") == 16);
	}
}
